package com.feverteam.neocrypto.binanceus.rest.kline;

import com.feverteam.neocrypto.binanceus.rest.Interval;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class KlineServiceTest {

  @Autowired KlineService klineService;

  @Test
  void fetchTheDefaultAboutOfKlines() {
    List<Kline> list = klineService.get("AVAXUSD", Interval.FIVE_MINUTE);
    assertEquals(500, list.size(), "The default number of klines should be 500");
  }

  @Test
  void getMaxLimit() {
    List<Kline> list = klineService.get("AVAXUSD", Interval.FIVE_MINUTE, null, null, 1000);
    assertEquals(1000, list.size(), "The total number of klines should be 1000");
  }
}
