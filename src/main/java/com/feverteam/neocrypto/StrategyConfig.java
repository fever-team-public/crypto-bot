package com.feverteam.neocrypto;

import com.feverteam.neocrypto.binanceus.socket.DataSocket;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;

@Configuration
@Slf4j
public class StrategyConfig {

  @Bean
  ApplicationRunner appRunner(DataSocket dataSocket) {
    return args ->
        CompletableFuture.runAsync(
            () -> {
              while (true) {
                final CountDownLatch latch = new CountDownLatch(1);
                dataSocket
                    .connect()
                    .doOnTerminate(
                        () -> {
                          log.debug("Socket connection terminated.");
                          latch.countDown();
                        })
                    .doOnError(
                        e -> {
                          log.debug("Socket connection errored.");
                          latch.countDown();
                        })
                    .subscribe();
                try {
                  latch.await();
                } catch (InterruptedException e) {
                  log.warn("Socket runner thread was interrupted.");
                  return;
                }
              }
            });
  }
}
