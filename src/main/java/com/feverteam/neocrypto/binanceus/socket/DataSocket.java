package com.feverteam.neocrypto.binanceus.socket;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.feverteam.neocrypto.binanceus.BinanceUsProperties;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
import org.springframework.web.reactive.socket.client.WebSocketClient;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;
import reactor.netty.http.client.WebsocketClientSpec;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class DataSocket {

  private final List<IncomingSocketMessageHandler> handlers;
  private final BinanceUsProperties properties;
  private final ObjectMapper mapper = new ObjectMapper();

  public DataSocket(
      Optional<List<IncomingSocketMessageHandler>> handlers, BinanceUsProperties properties) {
    this.handlers = handlers.orElseGet(ArrayList::new);
    this.properties = properties;
  }

  @SneakyThrows
  public Mono<Void> connect() {
    SocketMessage message =
        SocketMessage.builder()
            .id(1)
            .method("SUBSCRIBE")
            .params(List.of("avaxusd@kline_5m", "avaxusd@bookTicker"))
            .build();

    String initMessage = mapper.writeValueAsString(message);

    WebSocketClient client =
        new ReactorNettyWebSocketClient(
            HttpClient.create(),
            () -> WebsocketClientSpec.builder().maxFramePayloadLength(1024000));
    return client.execute(
        URI.create(properties.getFeedUrl()),
        session ->
            session
                .send(Mono.just(session.textMessage(initMessage)))
                .thenMany(
                    session
                        .receive()
                        .map(WebSocketMessage::getPayloadAsText)
                        .map(this::deserializeMessage)
                        .doOnNext(m -> handlers.forEach(h -> h.receive(m))))
                .then());
  }

  @SneakyThrows
  private IncomingSocketMessage deserializeMessage(String message) {
    return mapper.readValue(message, IncomingSocketMessage.class);
  }
}
