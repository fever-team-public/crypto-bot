package com.feverteam.neocrypto.binanceus.socket;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

/** Created by Joseph Terzieva on 4/17/20. */
@Getter
@Builder
public class SocketMessage {

  private int id;
  private String method;
  private List<String> params;
}
