package com.feverteam.neocrypto.binanceus.socket;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;

/** Created by Joseph Terzieva on 4/9/20. */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(as = KlineSocketMessage.class)
public class KlineSocketMessage implements IncomingSocketMessage {
  @JsonProperty("e")
  private String event;

  @JsonProperty("k")
  private KlineSocketMessageData data;
}
