package com.feverteam.neocrypto.binanceus.socket;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.Map;

public class IncomingSocketMessageDeserializer extends JsonDeserializer<IncomingSocketMessage> {

  @Override
  public IncomingSocketMessage deserialize(
      JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
    ObjectMapper mapper = (ObjectMapper) jsonParser.getCodec();
    ObjectNode root = mapper.readTree(jsonParser);

    if (root.has("e") && root.get("e").asText().equals("kline")) {
      return mapper.readValue(root.toString(), KlineSocketMessage.class);
    }

    if (!root.has("e")) {
      return mapper.readValue(root.toString(), BookTickerSocketMessage.class);
    }

    Map<String, Object> map = mapper.readValue(root.toString(), new TypeReference<>() {});
    UnknownSocketMessage message = new UnknownSocketMessage();
    message.setProperties(map);

    return message;
  }
}
