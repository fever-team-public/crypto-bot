package com.feverteam.neocrypto.binanceus.socket;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class KlineSocketMessageData {

  @JsonProperty("i")
  private String interval;

  @JsonProperty("o")
  private double open;

  @JsonProperty("x")
  private boolean closed;

  @JsonProperty("h")
  private double high;

  @JsonProperty("l")
  private double low;

  @JsonProperty("c")
  private double close;

  @JsonProperty("s")
  private String symbol;

  @JsonProperty("v")
  private double volume;
}
