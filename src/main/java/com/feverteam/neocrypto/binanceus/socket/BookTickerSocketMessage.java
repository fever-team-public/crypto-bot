package com.feverteam.neocrypto.binanceus.socket;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(as = BookTickerSocketMessage.class)
public class BookTickerSocketMessage implements IncomingSocketMessage {
    @JsonProperty("u")
    private long id;
    @JsonProperty("s")
    private String symbol;
    @JsonProperty("b")
    private double bidPrice;
    @JsonProperty("B")
    private double bidQuantity;
    @JsonProperty("a")
    private double askPrice;
    @JsonProperty("A")
    private double askQuantity;
}