package com.feverteam.neocrypto.binanceus.socket;

import lombok.Data;

import java.util.Map;

@Data
public class UnknownSocketMessage implements IncomingSocketMessage {

  private Map<String, Object> properties;
}
