package com.feverteam.neocrypto.binanceus.socket;

public interface IncomingSocketMessageHandler {
  void receive(IncomingSocketMessage message);
}
