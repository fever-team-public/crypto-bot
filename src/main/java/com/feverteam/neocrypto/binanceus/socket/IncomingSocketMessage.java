package com.feverteam.neocrypto.binanceus.socket;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(using = IncomingSocketMessageDeserializer.class)
public interface IncomingSocketMessage {}
