package com.feverteam.neocrypto.binanceus;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "binanceus")
public class BinanceUsProperties {

  private String feedUrl;
  private String restUrl;
}
