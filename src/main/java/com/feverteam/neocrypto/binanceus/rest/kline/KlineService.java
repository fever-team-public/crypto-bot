package com.feverteam.neocrypto.binanceus.rest.kline;

import com.feverteam.neocrypto.binanceus.rest.BinanceRestTemplate;
import com.feverteam.neocrypto.binanceus.rest.Interval;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class KlineService {

  private final BinanceRestTemplate restTemplate;
  private static final String endpointUri = "/api/v3/klines";

  public KlineService(BinanceRestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  public List<Kline> get(String symbol, Interval interval) {
    return get(symbol, interval, null, null, null);
  }

  public List<Kline> get(
      String symbol,
      Interval interval,
      ZonedDateTime startTime,
      ZonedDateTime endTime,
      Integer limit) {

    Map<String, Object> queryParams = new HashMap<>();
    queryParams.put("symbol", symbol);
    queryParams.put("interval", interval);

    String endpointWithQuery = endpointUri + "?symbol={symbol}&interval={interval}";

    if (startTime != null) {
      queryParams.put("startTime", startTime.toInstant().toEpochMilli());
      endpointWithQuery += "&startTime={startTime}";
    }

    if (endTime != null) {
      queryParams.put("endTime", endTime.toInstant().toEpochMilli());
      endpointWithQuery += "&endTime={endTime}";
    }

    if (limit != null) {
      queryParams.put("limit", limit);
      endpointWithQuery += "&limit={limit}";
    }

    return restTemplate
        .get(endpointWithQuery, queryParams, new ParameterizedTypeReference<List<List<Object>>>() {})
        .getBody()
        .stream()
        .map(this::toKline)
        .collect(Collectors.toList());
  }

  private Kline toKline(List<Object> entry) {
    return new Kline(
            ZonedDateTime.ofInstant(
                    Instant.ofEpochMilli((long)entry.get(0)),
                    ZoneOffset.UTC
            ),
            Double.parseDouble((String)entry.get(1)),
            Double.parseDouble((String)entry.get(2)),
            Double.parseDouble((String)entry.get(3)),
            Double.parseDouble((String)entry.get(4)),
            Double.parseDouble((String)entry.get(5)),
            ZonedDateTime.ofInstant(
                    Instant.ofEpochMilli((long)entry.get(6)),
                    ZoneOffset.UTC
            ),
            Double.parseDouble((String)entry.get(7)),
            (int)entry.get(8),
            Double.parseDouble((String)entry.get(9)),
            Double.parseDouble((String)entry.get(10))
    );
  }
}
