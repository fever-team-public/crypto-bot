package com.feverteam.neocrypto.binanceus.rest.kline;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.ZonedDateTime;

@Getter
@AllArgsConstructor
public class Kline {

    private ZonedDateTime openTime;
    private double open;
    private double high;
    private double low;
    private double close;
    private double volume;
    private ZonedDateTime closeTime;
    private double quoteAssetVolume;
    private int numberOfTrades;
    private double takerBuyBaseAssetVolume;
    private double takerBuyQuoteAssetVolume;

}
