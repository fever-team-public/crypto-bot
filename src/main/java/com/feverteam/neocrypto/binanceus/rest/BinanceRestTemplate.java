package com.feverteam.neocrypto.binanceus.rest;

import com.feverteam.neocrypto.binanceus.BinanceUsProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

/**
 * Low-level rest template configured to access the binance US endpoint.
 */
@Component
public class BinanceRestTemplate {

    private final RestTemplate restTemplate;

    public BinanceRestTemplate(BinanceUsProperties properties) {
        this.restTemplate = new RestTemplateBuilder()
                .rootUri(properties.getRestUrl())
                .build();
    }

    /**
     * Get a request from the Binance US API.
     * @param path path to the endpoint
     * @param urlVariables variables for the endpoint
     * @param clazz class of the response entity
     * @return entity containing the response of the request
     * @param <T> type returned from the response.
     */
    public <T> ResponseEntity<T> get(String path, Map<String,Object> urlVariables, ParameterizedTypeReference<T> clazz) {
        return restTemplate.exchange(path, HttpMethod.GET, voidEntity(), clazz, urlVariables);
    }

    private HttpEntity<Void> voidEntity() {
        MultiValueMap<String,String> headers = new LinkedMultiValueMap<>();
        return new HttpEntity<>(headers);
    }

}
