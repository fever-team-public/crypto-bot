package com.feverteam.neocrypto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class Position {
    private final double price;
    private final double quantity;

    @JsonCreator(mode=JsonCreator.Mode.PROPERTIES)
    public Position(@JsonProperty("price") double price, @JsonProperty("quantity") double quantity) {
        this.price = price;
        this.quantity = quantity;
    }

}
